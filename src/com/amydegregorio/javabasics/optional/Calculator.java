package com.amydegregorio.javabasics.optional;

import java.util.Optional;
import java.util.Random;

public class Calculator {
    public Optional<Long> divide(long dividend, long divisor) {
       if (divisor == 0) {
          return Optional.empty();
       }
       
       return Optional.of(dividend / divisor);
    }
    
    public Long random() {
       System.out.println("Generating a random long...");
       return new Random().nextLong();
    }
}
