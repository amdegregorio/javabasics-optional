package com.amydegregorio.javabasics.optional;

import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalDemo {    
   public static void main(String... args) {
      Calculator calc = new Calculator();
      
      Optional<Long> emptyQuotient = calc.divide(15, 0);
      Optional<Long> quotient = calc.divide(15, 3);
      
      System.out.println("emptyQuotient.isPresent(): " + emptyQuotient.isPresent());
      System.out.println("quotient.isPresent(): " + quotient.isPresent());
      
      System.out.println("emptyQuotient.isEmpty(): " + emptyQuotient.isEmpty());
      System.out.println("quotient.isEmpty(): " + quotient.isEmpty());
      
      System.out.println("quotient.get() " + quotient.get());
      
      try {
         System.out.println("emptyQuotient.orElseThrow() " + emptyQuotient.orElseThrow());
      } catch(NoSuchElementException e) {
         System.out.println("emptyQuotient.orElseThrow() "+ e.getMessage());
      }
      
      try {
         System.out.println("quotient.orElseThrow() " + emptyQuotient.orElseThrow(IllegalArgumentException::new));
      } catch(IllegalArgumentException e) {
         System.out.println("quotient.orElseThrow() " + e.getMessage());
      }
      
      System.out.println("emptyQuotient.orElse() " + emptyQuotient.orElse(0L));
      System.out.println("quotient.orElse() " + quotient.orElse(0L));
      
      System.out.println("emptyQuotient.orElseGet() " + emptyQuotient.orElseGet(() -> 0L));
      System.out.println("quotient.orElseGet() " + quotient.orElseGet(() -> 0L));
      
      System.out.println("emptyQuotient.orElse() " + emptyQuotient.orElse(calc.random()));
      System.out.println("quotient.orElse() " + quotient.orElse(calc.random()));
      
      System.out.println("emptyQuotient.orElseGet() " + emptyQuotient.orElseGet(() -> calc.random()));
      System.out.println("quotient.orElseGet() " + quotient.orElseGet(() -> calc.random()));      
      
      System.out.println("emptyQuotient.or() " + emptyQuotient.or(() -> Optional.of(0L)));
      System.out.println("quotient.or() " + quotient.or(() -> Optional.of(0L)));
      
      quotient.ifPresent((value) -> System.out.println("value * 6 = " + value * 6));
      
      emptyQuotient.ifPresentOrElse((value) -> System.out.println("Found: " + value),
             () -> System.out.println("No value returned"));
      
      quotient.ifPresentOrElse((value) -> System.out.println("Found: " + value),
               () -> System.out.println("No value returned"));
      
      
      System.out.println("quotient.filter() on 5 " + quotient.filter(value -> value == 5));
      System.out.println("quotient.filter() on 3 " + quotient.filter(value -> value == 3));       
      
      System.out.println(Optional.of(calc).map(c -> c.divide(15, 3)));
      System.out.println(Optional.of(calc).flatMap(c -> c.divide(15, 3)));
      
      System.out.println(quotient.stream().anyMatch((q) -> q == 5));
   }
}
