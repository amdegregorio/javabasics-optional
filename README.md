# JavaBasics: Optional
Example code for Java Basics: Optional post

For more information see [Java Basics: Optional](https://amydegregorio.com/2021/01/10/java-basics-optional/)

## Examples

Run the OptionalDemo main method to see the examples run.


#### License

This project is licensed under the Apache License version 2.0.  
See the LICENSE file or go to [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) for more information. 
